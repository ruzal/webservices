* install Ubuntu
* install docker,  git, docker-compose for Ubuntu
* In terminal (Ctrl+Alt+T)
* git clone https://gitlab.com/ruzal/webservices.git
* cd webservices
* docker-compose up -d
* result - something like
```WARNING: The UID variable is not set. Defaulting to a blank string.
WARNING: The GID variable is not set. Defaulting to a blank string.
Removing webservices_nginx_1
webservices_mysql_1 is up-to-date
webservices_php_1 is up-to-date
```
* [http://127.0.0.1](http://127.0.0.1) - site deployed

* docker ps - find 32bitforkwebservicesagency_php_1 id
```CONTAINER ID        IMAGE                            COMMAND                  CREATED             STATUS              PORTS                                      NAMES
b87785376a7c        webservices_php   "docker-php-entrypoi…"   40 seconds ago      Up 36 seconds       9000/tcp
```
* docker exec -it b87785376a7c /bin/bash
* composer update 
* edit file /www/project/.env 
```
DATABASE_URL=mysql://root:root@mysql:3306/au?serverVersion=5.7
```
* php bin/console doctrine:database:create
* php bin/console doctrine:migrations:migrate
* Thats all. you can exit and change code on host machine directory /www/project and see results at [http://127.0.0.1](http://127.0.0.1)

* Also you can do this:
* edit file: sudo nano /etc/hosts
* 127.0.0.1 	webservices
* Ctrl+O Enter Ctrl+X
* [http://127.0.0.1](http://127.0.0.1)http://webservices - site deployed